<div class="modal fade" id="salesModal" tabindex="-1" role="dialog" aria-labelledby="salesModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="salesFormModal">
                <div class="modal-header">
                    <h5 class="modal-title" id="salesModalLabel">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body clearfix">
                    <input type="hidden" name="id">
                    <label for="" class="label-control font-weight-bold">Date</label>
                    <input type="date" name="date" class="form-control">
                    <table class="table table-hover mt-4" id="categoryTable">
                        <thead>
                            <tr>
                                <th scope="col">Category</th>
                                <th scope="col">Amount</th>
                            </tr>
                        </thead>
                        <tbody>     
                        </tbody>
                    </table> 
                    <div class="float-right lead">
                        <label>Total:</label>
                        <label id="lblTotalAmount">$0.00</label>
                    </div>       
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>