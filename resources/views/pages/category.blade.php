@extends("layouts.app")

@section("content")
    <div class="container mt-3">
        @include('modal.category')
        <div class="card card-body">
            <div class="clearfix">
                <p class="lead float-left">Categories</p>
                <button class="btn btn-outline-info btn-sm float-right" data-toggle="modal" data-target="#categoryModal" data-action="create">Add Item</button>
            </div>
            <table class="table table-hover" id="categoryTable">
                <thead>
                    <tr>
                    <th scope="col">Name</th>
                    <th scope="col">Actions</th>
                    </tr>
                </thead>
                <tbody>     
                </tbody>
            </table> 
        </div>                 
    </div>
@endsection

@section('script-js')
    <script src="{{ asset('/js/category.js')}}"></script>
@endsection