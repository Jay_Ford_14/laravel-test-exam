@extends("layouts.app")

@section("content")
    <div class="container mt-3">
        @include('modal.sales')
        <div class="card card-body">
            <div class="clearfix">
                <p class="lead float-left">Sales</p>
                <button class="btn btn-outline-info btn-sm float-right" data-toggle="modal" data-target="#salesModal" data-action="create">Add Item</button>
            </div>
            <table class="table table-hover" id="salesTable">
                <thead>
                    <tr>
                    <th scope="col">Date</th>
                    <th scope="col">Total</th>
                    <th scope="col">Actions</th>
                    </tr>
                </thead>
                <tbody>     
                </tbody>
            </table> 
        </div>                 
    </div>
@endsection

@section('script-js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.10/lodash.min.js"></script>
    <script src="{{ asset('/js/sales.js')}}"></script>
@endsection