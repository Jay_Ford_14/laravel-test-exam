'use strict'

const methods = {
    prefix : "/api/sales/",
    categories : [],
    listSales : function()
    {
        let self = this
        $.get(self.prefix + "list")
        .then(function( response){
            let data = response.data
            let table = $("#salesTable")
            let tbody = table.find("tbody")
            tbody.html("")
            if(! data.length)
            {
                tbody.html('<tr><td colspan="3" class="text-center">No data found.</td></tr>')
            }
            $.each(data, function(key, item){
                self.categories.push({ id : item.id, amount : 0.0 })
                tbody.append(
                '<tr>\
                    <td>' + item.date + '</td>\
                    <td> ' + item.total + '</td>\
                    <td>\
                        <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#salesModal" data-action="update" data-id="' + item.id + '">Edit</button>\
                        <button class="btn btn-danger btn-sm"  onclick="methods.destroySale(' + item.id + ')">Delete</button>\
                    </td>\
                </tr>')
            })
        })
    },
    listCategory : function(id)
    {
        let self = this
        $.get("/api/category/list")
        .then(function(response){
            let data = response.data
            let table = $("#categoryTable")
            let tbody = table.find("tbody")
            tbody.html("")
            if(! data.length)
            {
                tbody.html('<tr><td colspan="2" class="text-center">No data found.</td></tr>')
            }
            $.each(data, function(key, item){
                self.categories.push({ category_id : item.id, amount : 0.0 })
                tbody.append(
                '<tr>\
                    <td>' + item.name + '</td>\
                    <td><input type="text" placeholder="0.00" class="form-control" onkeyup="methods.onAmountType(this)" data-id="' + item.id + '"></td>\
                </tr>')
            })
            if(id)
            {
                self.editSales(id)
            }
        })
    },
    onAmountType : function(e)
    {
        let $this = $(e)
        let id = $this.data("id")
        let amount = parseFloat($this.val() || 0)
        let $data = methods.categories
        let returnData = _.find($data, { category_id : id })
            returnData.amount = amount
        this.computeVal()
    },
    computeVal : function()
    {
        let totalAmount = 0
        $.each(methods.categories, function(key, item){
            totalAmount += item.amount
        })
        $("#lblTotalAmount").text("$" + (totalAmount.toFixed(2)))
    },
    saveSales : function(form, modal, action)
    {
        let self = this
        $.post(self.prefix + action, {
            id : $('[name="id"]').val(),
            date : $('[name="date"]').val(),
            items : methods.categories 
        })
        .then(function( response){
            modal.modal("hide")
            toastr.success(response.success, "Success")
            self.listSales()
        })
    },
    editSales : function(id)
    {
        let self = this
        $.get(self.prefix + "edit/" + id)
        .then(function(response){
            $('[name="id"').val(response.id)
            $('[name="date"]').get(0).valueAsDate = new Date(response.date)
            let elem = $("#categoryTable tbody tr td")
            let items = response.items
            $.each(items, function(key, item){
                let cat_id = item.category_id
                let amnt = item.amount
                _.find(self.categories, { category_id : cat_id }).amount = amnt
                elem.find("[data-id='" + cat_id + "']").val(amnt)
            })
            self.computeVal()
        })
    },
    destroySale : function(id)
    {
        let self = this
        $.confirm({
            title: "Delete Sale",
            content: "Are you sure to delete this?",
            type: 'red',
            typeAnimated: true,
            buttons: {
                ok: {
                    text: "Ok",
                    btnClass: 'btn-red',
                    action: function(){
                        $.post( self.prefix + "destroy", { id : id })
                        .then(function(response){
                            self.listSales()
                            toastr.success(response.success,"Success")
                        })
                        .catch(function(error){})
                    }
                },
                close: {
                    text : "Close",
                    action : function()
                    {

                    }
                }
            }
        })
    },
    resetData : function()
    {
        $('[name="date"]').get(0).valueAsDate = new Date()
        methods.categories = []
        this.computeVal()
    }
}

methods.listSales()

let modal = $("#salesModal")
let form = $("#salesFormModal")
let action = "create"

modal.on("show.bs.modal", function(e){
    let elem = $(e.relatedTarget)
    action = elem.data('action')
    $("#salesModalLabel").html(action == "create"? "Create Sale" : "Edit Sale")
    methods.resetData()
    methods.listCategory(elem.data("id"))
}) 

form.submit(function(e){
    e.preventDefault()
    methods.saveSales($(this), modal, action)
})

$(document).on("keypress", "#categoryTable input", function(event) {
    if(event.which == 13)
    {
        return true;
    }
    if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
        event.preventDefault()
    }
})

