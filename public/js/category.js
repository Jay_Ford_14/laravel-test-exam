'use strict'

const methods = {
    prefix : "/api/category/",
    listCategory : function()
    {
        let self = this
        $.get(self.prefix + "list")
        .then(function(response){
            let data = response.data
            let table = $("#categoryTable")
            let tbody = table.find("tbody")
            tbody.html("")
            if(! data.length)
            {
                tbody.html('<tr><td colspan="2" class="text-center">No data found.</td></tr>')
            }
            $.each(data, function(key, item){
                tbody.append(
                '<tr>\
                    <td> ' + item.name + '</td>\
                    <td>\
                        <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#categoryModal" data-action="update" onclick="methods.editCategory(' + item.id + ')">Edit</button>\
                        <button class="btn btn-danger btn-sm"  onclick="methods.destroyCategory(' + item.id + ')">Delete</button>\
                    </td>\
                </tr>')
            })
        })
    },
    saveCategory : function(form, modal, action)
    {
        let self = this
        $.post(self.prefix + action, form.serialize())
        .then( function(response){
            toastr.success(response.success, "Success")
            modal.modal("hide")
            self.listCategory()
        })
        .catch( function(err){
            console.log(err, "err")
        })
    },
    clearInputs : function(form)
    {
        form.find(".modal-body :input").val("")
    },
    editCategory : function(id)
    {
        let self = this
        let form = $("#categoryFormModal")
        $.get(self.prefix + "edit/" + id)
        .then( function(response){
            form.find(".modal-body :input").each(function(){
                let $this = $(this)
                let name = $this.attr("name")
                $this.val(response[name])
            })
        })
    },
    destroyCategory : function(id)
    {
        let self = this
        $.confirm({
            title: "Delete Category",
            content: "Are you sure to delete this?",
            type: 'red',
            typeAnimated: true,
            buttons: {
                ok: {
                    text: "Ok",
                    btnClass: 'btn-red',
                    action: function(){
                        $.post( self.prefix + "destroy", { id : id })
                        .then(function(response){
                            self.listCategory()
                            toastr.success(response.success,"Success")
                        })
                        .catch(function(error){})
                    }
                },
                close: {
                    text : "Close",
                    action : function()
                    {

                    }
                }
            }
        })
    }
}


methods.listCategory()

let modal = $("#categoryModal")
let form = $("#categoryFormModal")
let action = "create"

modal.on("show.bs.modal", function(e){
    let elem = $(e.relatedTarget)
    action = elem.data('action')
    $("#categoryModalLabel").html(action == "create"? "Create Category" : "Edit Category")
})

modal.on('hidden.bs.modal', function(){
    methods.clearInputs(form)
})

form.submit(function(e){
    e.preventDefault()
    methods.saveCategory($(this), modal, action)
})