<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Resources\CategoryResource;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function create(Request $request)
    {
        $data = $request->only("name");
        if(! Category::create($data))
        {
            return response()->json(['error' => "Error: Something went wrong."], 500);
        }
        return response()->json(['success' => "Category has been successfully added."], 200);
    }

    public function list()
    {
        return CategoryResource::collection(Category::orderBy("created_at", "desc")->get());
    }

    public function edit($id)
    {
        return Category::find($id);
    }

    public function update(Request $request)
    {
        $data = $request->only("name");
        if(! Category::find($request->id)->update($data))
        {
            return response()->json(['error' => "Error: Something went wrong."], 500);
        }
        return response()->json(['success' => "Category has been successfully updated."], 200);
    }

    public function destroy(Request $request)
    {
        if(! Category::destroy($request->id))
        {
            return response()->json(['error' => "Error: Something went wrong."], 500);
        }
        return response()->json(['success' => "Category has been successfully deleted."], 200);
    }
}
