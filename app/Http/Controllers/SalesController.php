<?php

namespace App\Http\Controllers;

use App\Sale;
use App\SalesItem;

use DB;
use App\Http\Resources\SaleResource;
use Illuminate\Http\Request;

class SalesController extends Controller
{
    public function create(Request $request)
    {
        DB::beginTransaction();
        $data = $request->only('date');
        $sales = Sale::create($data);
        if(! $sales)
        {
            DB::rollback();
            return response()->json(['error' => "Error: Something went wrong."], 500);
        }
        $items = $request->items;
        $this->saveSalesItem($sales, $items);
        DB::commit();
        return response()->json(['success' => "Sale has been successfully added."], 200);
    }

    private function saveSalesItem($sales, $items)
    {
        foreach ($items as $key => $item) {
            if(! SalesItem::create([
                'sales_id' => $sales->id,
                'category_id' => $item['category_id'],
                'amount' => $item['amount'],
            ]))
            {
                DB::rollback();
                return response()->json(['error' => "Error: Something went wrong."], 500);
            }
        }
    }

    public function list()
    {
        return SaleResource::collection(Sale::orderBy("created_at", "desc")->get());
    }

    public function edit($id)
    {
        return Sale::find($id)->load('items');
    }

    public function update(Request $request)
    {
        DB::beginTransaction();
        $data = $request->only('date');
        $sales  = Sale::find($request->id);
        if(! $sales->update($data))
        {
            DB::rollback();
            return response()->json(['error' => "Error: Something went wrong."], 500);
        }
        if(! $sales->items()->delete())
        {
            DB::rollback();
            return response()->json(['error' => "Error: Something went wrong."], 500); 
        }
        $items = $request->items;
        $this->saveSalesItem($sales, $items);
        DB::commit();
        return response()->json(['success' => "Sale has been successfully updated."], 200);
    }

    public function destroy(Request $request)
    {
        if(! Sale::destroy($request->id))
        {
            return response()->json(['error' => "Error: Something went wrong."], 500);
        }
        return response()->json(['success' => "Sale has been successfully deleted."], 200);
    }
}
