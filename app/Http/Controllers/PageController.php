<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function index()
    {
        return view("pages.index");
    }

    public function category()
    {
        return view("pages.category");
    }

    public function sales()
    {
        return view("pages.sales");
    }
}
