<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    protected $fillable = [ 
        "date"
    ];

    public function items()
    {
        return $this->hasMany('App\SalesItem', 'sales_id');
    }
}
