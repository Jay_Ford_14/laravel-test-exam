<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', "PageController@index");
Route::get('/category', "PageController@category");
Route::get('/sales', "PageController@sales");

Route::group([
    'prefix' => 'api'
], function() {
    
    Route::group([
        'prefix' => 'category'
    ], function() {
        Route::get("list", "CategoryController@list");
        Route::post("create", "CategoryController@create");
        Route::get("edit/{id}", "CategoryController@edit");
        Route::post("update", "CategoryController@update");
        Route::post("destroy", "CategoryController@destroy");
    });

    Route::group([
        'prefix' => 'sales'
    ], function() {
        Route::get("list", "SalesController@list");
        Route::post("create", "SalesController@create");
        Route::get("edit/{id}", "SalesController@edit");
        Route::post("update", "SalesController@update");
        Route::post("destroy", "SalesController@destroy");
    });
    
});
